
# API REST en Node.js con Express

Este es un proyecto de ejemplo de una API REST desarrollada en Node.js utilizando el framework Express. La API ofrece operaciones matemáticas básicas y se puede utilizar como punto de partida para proyectos más complejos.

## Instalación

1. Asegúrate de tener [Node.js](https://nodejs.org/) instalado en tu sistema.

2. Clona este repositorio o descarga los archivos del proyecto.

3. En la raíz del proyecto, ejecuta el siguiente comando para instalar las dependencias:

   ```bash
   npm install


  Tarea | Tablas RESTful

 Crear API REST utilizando la siguiente tabla RESTFull:

       GET /results/:n1/:n2 -> Sumar n1 + n2
       POST /results/ -> Multiplicar n1 * n2
       PUT /results/ -> Dividir n1 / n2
       PATCH /results/ -> Potencia n1 ^ n2
       DELETE /results/:n1/:n2 -> restar n1 - n2

   Gabriel Isai Prieto Saenz 353297
   Docente: Luis MArtinez Ramirez
