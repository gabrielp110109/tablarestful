const express = require('express');

const app = express();
const port = 3000;

app.use(express.json());

app.get('/results/:n1/:n2', (req, res) => {
  const { n1, n2 } = req.params;
  const result = parseFloat(n1) + parseFloat(n2);
  res.json({ result });
});


app.post('/results', (req, res) => {
  const { n1, n2 } = req.body;
  const result = parseFloat(n1) * parseFloat(n2);
  res.json({ result });
});


app.put('/results', (req, res) => {
  const { n1, n2 } = req.body;
  if (parseFloat(n2) === 0) {
    res.status(400).json({ error: 'No se puede dividir por cero' });
  } else {
    const result = parseFloat(n1) / parseFloat(n2);
    res.json({ result });
  }
});

app.patch('/results', (req, res) => {
  const { n1, n2 } = req.body;
  const result = Math.pow(parseFloat(n1), parseFloat(n2));
  res.json({ result });
});

app.delete('/results/:n1/:n2', (req, res) => {
  const { n1, n2 } = req.params;
  const result = parseFloat(n1) - parseFloat(n2);
  res.json({ result });
});

app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});
